package com.kuyun.auditing.rest.constants;

public class ErrorCode {

	public final static int ILLEGAL_USER = 1000;
	
	public final static int MISSING_PARAMS = 1001;
	
	public final static int ILLEGAL_PARAMS = 1002;
	
	public final static int SMS_INTERVAL_SHORT = 2001;
	public final static int SMS_SEND_ERROR = 2002;
	
	//git test 2
	
}
