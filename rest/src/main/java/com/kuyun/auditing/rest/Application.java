package com.kuyun.auditing.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.kuyun.auditing.rest.interceptor.ResponseDataInterceptor;

@Configuration
@EnableAutoConfiguration
@ConfigurationProperties
@PropertySource("classpath:resource.properties") 
@ComponentScan
public class Application extends WebMvcConfigurerAdapter {
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		super.addInterceptors(registry);
		registry.addInterceptor(new ResponseDataInterceptor());
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class);
	}

}
