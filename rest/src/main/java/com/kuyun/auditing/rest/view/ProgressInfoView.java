package com.kuyun.auditing.rest.view;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL) 
public class ProgressInfoView {

	@JsonProperty("material_count")
	private int materialCount;
	
	@JsonProperty("audited_count")
	private int auditedCount;
	
	@JsonProperty("wait_audit_count")
	private int waitAuditCount;

	public int getMaterialCount() {
		return materialCount;
	}

	public void setMaterialCount(int materialCount) {
		this.materialCount = materialCount;
	}

	public int getAuditedCount() {
		return auditedCount;
	}

	public void setAuditedCount(int auditedCount) {
		this.auditedCount = auditedCount;
	}

	public int getWaitAuditCount() {
		return waitAuditCount;
	}

	public void setWaitAuditCount(int waitAuditCount) {
		this.waitAuditCount = waitAuditCount;
	}
	
}
