package com.kuyun.auditing.rest.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ky_ad_auditing_result")
public class AuditingResult {
	
	@Id
    @GeneratedValue
	private int id;
	
	@ManyToOne
	@JoinColumn(name="resourceId")
	private Material material;
	
	@Column
	private int status;
	
	@Column
	private int auditingUser;
	
	@Column
	private String failReason;
	
	@Column
	private Integer refuse;
	
	@Column
	private Date updateTime;
	
	@Column
	private String exculdeTime;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getAuditingUser() {
		return auditingUser;
	}

	public void setAuditingUser(int auditingUser) {
		this.auditingUser = auditingUser;
	}

	public String getFailReason() {
		return failReason;
	}

	public void setFailReason(String failReason) {
		this.failReason = failReason;
	}

	public Integer getRefuse() {
		return refuse;
	}

	public void setRefuse(Integer refuse) {
		this.refuse = refuse;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public String getExculdeTime() {
		return exculdeTime;
	}

	public void setExculdeTime(String exculdeTime) {
		this.exculdeTime = exculdeTime;
	}
	
}
