package com.kuyun.auditing.rest.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * cookie工具类
 */
public class CookieUtil {

	/**
	 * 设置Cookie
	 * 
	 * @param key
	 *            要设置的Cookie名字
	 * @param value
	 *            要设置的Cookie值
	 * @param maxAge
	 *            设置Cookie的有效期
	 * @param res
	 *            HttpServletResponse对象
	 */
	public static void setCookie(String key, String value, int maxAge, HttpServletRequest req,
			HttpServletResponse res) {
		setCookie(key, value, maxAge, req, res, null, null);
	}

	/**
	 * 设置Cookie
	 * 
	 * @param key
	 *            要设置的Cookie名字
	 * @param value
	 *            要设置的Cookie值
	 * @param maxAge
	 *            设置Cookie的有效期
	 * @param res
	 *            HttpServletResponse对象
	 * @param domain
	 *            设置Cookie的domain
	 * @param path
	 *            设置Cookie的path
	 */
	public static void setCookie(String key, String value, int maxAge, HttpServletRequest req, HttpServletResponse res,
			String domain, String path) {
		if (null == key || key.trim().equals("") || res == null) {
			return;
		}
		Cookie cookie = null;
		cookie = getCookieEntity(key, req);
		if (cookie == null) {
			cookie = new Cookie(key, value);
		} else {
			cookie.setValue(value);
		}
		cookie.setMaxAge(maxAge);
		if (domain != null) {
			cookie.setDomain(domain);
		}
		if (path != null) {
			cookie.setPath(path);
		}
		res.addCookie(cookie);
	}

	/**
	 * 获取 Cookie
	 * 
	 * @param key
	 *            要获取的Cookie的名字
	 * @param req
	 *            HttpServletRequest对象
	 * @return
	 */
	public static Cookie getCookieEntity(String key, HttpServletRequest req) {
		if (null == key || key.trim().equals("") || req == null) {
			return null;
		}
		Cookie[] cookies = req.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (key.equals(cookies[i].getName())) {
					return cookies[i];
				}
			}
		}
		return null;
	}

	/**
	 * 获取Cookie
	 * 
	 * @param key
	 *            要获取的Cookie名字
	 * @param req
	 *            HttpServletRequest对象
	 * @return
	 */
	public static String getCookie(String key, HttpServletRequest req) {
		if (null == key || key.trim().equals("") || req == null) {
			return null;
		}
		Cookie[] cookies = req.getCookies();

		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (key.equals(cookies[i].getName())) {
					return cookies[i].getValue();
				}
			}
		}
		return null;
	}

	/**
	 * 清除Cookie
	 * 
	 * @author
	 * @param res
	 *            HttpServletResponse对象
	 */
	public static void cleanCookies(HttpServletRequest req, HttpServletResponse res) {
		cleanCookie(req, res, null, null);
	}

	/**
	 * 清除Cookie
	 * 
	 * @param res
	 *            HttpServletResponse对象
	 * @param domain
	 *            设置Cookie的domain
	 * @param path
	 *            设置Cookie的path
	 */
	public static void cleanCookie(HttpServletRequest req, HttpServletResponse res, String domain, String path) {
		Cookie[] cookies = req.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = new Cookie(cookies[i].getName(), null);
				cookie.setMaxAge(0);
				if (domain != null) {
					cookie.setDomain(domain);
				}
				if (path != null) {
					cookie.setPath(path);
				}
				res.addCookie(cookie);
			}
		}
	}
}