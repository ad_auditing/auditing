package com.kuyun.auditing.rest.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.kuyun.auditing.rest.domain.AuditingResult;

public interface AuditingResultRepository extends PagingAndSortingRepository<AuditingResult,Integer>{

	AuditingResult findByMaterialIdAndAuditingUser(int materialId,int userId);
	
}
