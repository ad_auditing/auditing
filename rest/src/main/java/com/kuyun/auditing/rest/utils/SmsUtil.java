package com.kuyun.auditing.rest.utils;

public class SmsUtil {
	
	public static String genVerificationCode(){
		return String.valueOf((int)(Math.random()*9000+1000));
	}

	public  static boolean sms(String mobile,String content){
//		【酷云】
		String param = String.format("mobile=%s&content=%s", mobile,content);
		try {
			HttpClientUtil.sendPost("http://sms.in.kuyun.com/sms.do", param, false);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
//		System.out.println(genVerificationCode());
		System.out.println(String.format("mobile=%s&content=%s", 1381058032,"验证码"));
	}
}
