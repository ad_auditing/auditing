package com.kuyun.auditing.rest.view;

import java.text.SimpleDateFormat;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class ResponseBase<T> {

	
	@JsonProperty("api_version")
	String apiVersion = "1.0.0";
	
	String timestamp = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
	
	@JsonProperty("result_code")
	int resultCode;
	T data;

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public int getResultCode() {
		return resultCode;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
