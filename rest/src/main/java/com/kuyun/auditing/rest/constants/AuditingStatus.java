package com.kuyun.auditing.rest.constants;

public enum AuditingStatus {
	pending(1,"待审核"),approval(2,"审核通过"),refuse(3,"审核拒绝"),anew(4,"申请重审"); 
	AuditingStatus(int code,String value){
		this.code = code;
		this.value = value;
	}
	private int code;
	
	private String value;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
