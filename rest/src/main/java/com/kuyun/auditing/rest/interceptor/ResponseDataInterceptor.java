package com.kuyun.auditing.rest.interceptor;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuyun.auditing.rest.constants.ErrorCode;
import com.kuyun.auditing.rest.utils.AESCipher;
import com.kuyun.auditing.rest.utils.CookieUtil;
import com.kuyun.auditing.rest.view.ResponseBase;
import com.kuyun.auditing.rest.view.ResponseStatus;
import com.kuyun.auditing.rest.view.ResponseWrapper;

public class ResponseDataInterceptor implements HandlerInterceptor {
	
	AESCipher aesCipher = new AESCipher("ad_auditing");
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		HandlerMethod method = (HandlerMethod)handler;
		String invokeMethod = method.getMethod().getName();
		if(invokeMethod.equals("login") || invokeMethod.equals("logout")){
			return true;
		}
		String userName = CookieUtil.getCookie("identity", request);
		if(userName == null || aesCipher.decrypt(userName) == null){
			 PrintWriter out = response.getWriter(); 
			 ObjectMapper mapper = new ObjectMapper();
			 ResponseBase<ResponseStatus> responseData = new ResponseBase<>();
			 ResponseWrapper<ResponseStatus> res = new ResponseWrapper<>(responseData);
			 responseData.setResultCode(ErrorCode.ILLEGAL_USER);
			 String json = mapper.writeValueAsString(res);  
             out.print(json);  
             return false;
		}
		return true;
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
	}
}
