package com.kuyun.auditing.rest.dao;


import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.kuyun.auditing.rest.domain.Material;

public interface MaterialRepository extends PagingAndSortingRepository<Material, Integer> {
	
	public Page<Material> findAll(Pageable pageable);
	
	@Query("select material from Material material join material.auditingResult ar where ar.status=?1 and ar.auditingUser=?2")
	public Page<Material> findByResultStatus(int status ,int userId,Pageable pageable);
	
	public Page<Material> findByAuditingResultIsNull(Pageable pageable);
	
	/**
	 * 今日
	 * @param userId
	 * @param toady
	 * @param tommrow
	 * @return
	 */
	public int countByAuditingResultAuditingUserAndAuditingResultUpdateTimeAfterAndAuditingResultUpdateTimeBefore(int userId,Date toady,Date tommrow);
	/**
	 * 累计审核
	 * @return
	 */
	public int countByAuditingResultAuditingUser(int userId);
	
}
