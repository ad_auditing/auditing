package com.kuyun.auditing.rest.view;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserInfoView {

	@JsonProperty("user_id")
	private int userId;
	
	@JsonProperty("user_name")
	private String userName;
	
	@JsonProperty("mobile_number")
	private String mobileNumber;
	
	@JsonProperty("role_name")
	private String roleName;
	
	private String remarks;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
