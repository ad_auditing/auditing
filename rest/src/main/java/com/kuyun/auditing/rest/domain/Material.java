package com.kuyun.auditing.rest.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ky_ad_material")
public class Material {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column
	private String materialName;

	@Column
	private String url;

	@Column
	private int location;

	@Column
	private Integer linkId;

	@OneToMany(mappedBy="material")
	private List<AuditingResult> auditingResult;
	
	@Column
	private String genPicUrl;

	// @ManyToMany
	// @JoinTable(name="ky_ad_auditing_result",joinColumns=@JoinColumn(name="resourceId"),inverseJoinColumns=@JoinColumn(name="auditingUser"))
	// private Set<UserInfo> auditingUser;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public Integer getLinkId() {
		return linkId;
	}

	public void setLinkId(Integer linkId) {
		this.linkId = linkId;
	}

	public List<AuditingResult> getAuditingResult() {
		return auditingResult;
	}

	public void setAuditingResult(List<AuditingResult> auditingResult) {
		this.auditingResult = auditingResult;
	}

	public String getGenPicUrl() {
		return genPicUrl;
	}

	public void setGenPicUrl(String genPicUrl) {
		this.genPicUrl = genPicUrl;
	}

	// public Set<UserInfo> getAuditingUser() {
	// return auditingUser;
	// }
	//
	// public void setAuditingUser(Set<UserInfo> auditingUser) {
	// this.auditingUser = auditingUser;
	// }

}
