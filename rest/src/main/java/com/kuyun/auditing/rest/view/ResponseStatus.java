package com.kuyun.auditing.rest.view;

public class ResponseStatus {

	private int status;
	
	private String message = "失败";
	
	public ResponseStatus() {
		this(0);
	}
	
	public ResponseStatus(int status,String message){
		this.status = status;
		this.message = message;
	}
	
	public ResponseStatus(int status) {
		this.status = status;
		if(status == 0){ 
			message = "成功。";
		}
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
