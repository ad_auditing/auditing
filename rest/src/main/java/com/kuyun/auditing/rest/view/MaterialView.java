package com.kuyun.auditing.rest.view;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL) 
public class MaterialView {
	
	@JsonProperty("material_id")
	private int materialId;
	
	@JsonProperty("material_img")
	private String materialImg;
	
	@JsonProperty("audit_status")
	private int auditStatus;
	
	@JsonProperty("audit_time")
	private String auditTime;
	
	private String price;
	
	private String charging;

	public int getMaterialId() {
		return materialId;
	}

	public void setMaterialId(int materialId) {
		this.materialId = materialId;
	}

	public String getMaterialImg() {
		return materialImg;
	}

	public void setMaterialImg(String materialImg) {
		this.materialImg = materialImg;
	}

	public int getAuditStatus() {
		return auditStatus;
	}

	public void setAuditStatus(int auditStatus) {
		this.auditStatus = auditStatus;
	}

	public String getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(String auditTime) {
		this.auditTime = auditTime;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getCharging() {
		return charging;
	}

	public void setCharging(String charging) {
		this.charging = charging;
	}
	
}
