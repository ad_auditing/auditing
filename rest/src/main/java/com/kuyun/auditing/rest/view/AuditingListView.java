package com.kuyun.auditing.rest.view;

import java.util.List;
import java.util.Map;;

public class AuditingListView {
	private Map<String,Map<Integer,String>> options;
	private int size;
	private int pageNumber;
	private int totalPages;
	private List<MaterialView> list;
	public Map<String, Map<Integer, String>> getOptions() {
		return options;
	}
	public void setOptions(Map<String, Map<Integer, String>> options) {
		this.options = options;
	}
	public List<MaterialView> getList() {
		return list;
	}
	public void setList(List<MaterialView> list) {
		this.list = list;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
}
