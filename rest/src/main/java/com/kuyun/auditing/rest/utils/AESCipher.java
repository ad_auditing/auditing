package com.kuyun.auditing.rest.utils;
/*
 * @(#)AESCipher.java
 * 
 * Create Version: 1.0.0
 * Author: James Liu
 * Create Date: 2015-03-29
 * 
 * Copyright (c) 2015 Liushan. All Right Reserved.
 */
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;


/**
 * 包含扰码的AES加密解密。
 * MD5的16字节，被拆为4个长度部分：5 + 2 + 6 + 3。
 * 在不同的地方插入扰码：扰码 + 5(MD5) + 扰码 + 2(MD5) + 数据 + 6(MD5) + 扰码 + 3(MD5) + 扰码
 * 加密的Cookie值 = Base64UrlSafe(AES(扰码 + 5(MD5) + 扰码 + 2(MD5) + 数据 + 6(MD5) + 扰码 + 3(MD5) + 扰码))
 * 注：旧版本不是Base64UrlSafe，而是Hex，之所以替换是因为字符串可以平均缩短33%。
 * 
 * 注意：SecureRandom实现完全随操作系统本身的內部状态，除非调用方在调用getInstance方法，然后调用setSeed方法；
 * 该实现在windows上每次生成的key都相同，但是在solaris或部分linux系统上则不同。
 * 
 * @version 1.0.0   2015-02-13
 * @author  James Liu
 */
public class AESCipher {
    
    private static final char[] HEX_CHARS               = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static final String HEX_STRING              = "0123456789abcdef";
    private static final String RANDOM_CHARS            = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String CHARSET                 = "UTF-8";
    private static final String KEY_ALGORITHM           = "AES";
    //private static final int KEY_LENGTH                 = 128;
    private static final String SECURE_RANDOM_ALGORITHM = "SHA1PRNG";
    private static final String CIPHER_ALGORITHM        = "AES";    // "AES/ECB/PKCS5Padding";
    private static final String DIGEST_ALGORITHM        = "MD5";
    private static final int DIGEST_LENGTH              = 16;
    
    private volatile Key key;
    private volatile int scrambleLength = 0;    // 随机扰码长度
    
    
    /**
     * 默认构造方法。
     */
    public AESCipher() {}
    
    /**
     * 使用给定的密钥构造对象，扰码长度是0。
     * 
     * @param   secretKey       密钥
     */
    public AESCipher(String secretKey) {
        
        this.setSecretKey(secretKey);
    }
    
    /**
     * 使用给定的密钥和扰码长度构造对象。
     * 
     * @param   secretKey       密钥
     * @param   scrambleLength  扰码长度
     */
    public AESCipher(String secretKey, int scrambleLength) {
        
        this.setSecretKey(secretKey);
        this.scrambleLength = scrambleLength;
    }
    
    /**
     * 设置密钥。
     * 
     * @param   secretKey       密钥
     */
    public void setSecretKey(String secretKey) {
        
        if (secretKey == null) throw new NullPointerException("Secret key is null!");
        if (secretKey.length() == 0) throw new IllegalArgumentException("Secret key is not set!");
        
        byte[] secretKeyBytes;
        try {
            secretKeyBytes = secretKey.getBytes(CHARSET);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(CHARSET + " is not supported!", e);
        }
        
        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KEY_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(KEY_ALGORITHM + " is not supported!", e);
        }
        
        /* 在Linux上SecureRandom每次生成的Key不相同，故下面注释的语句不可用，需要明确指明安全随机数算法和种子才可以。 */
        //keyGenerator.init(KEY_LENGTH, new SecureRandom(secretKeyBytes));
        SecureRandom secureRandom;
        try {
            secureRandom = SecureRandom.getInstance(SECURE_RANDOM_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(SECURE_RANDOM_ALGORITHM + " is not supported!", e);
        }
        secureRandom.setSeed(secretKeyBytes);
        keyGenerator.init(secureRandom);
        
        this.key = new SecretKeySpec(keyGenerator.generateKey().getEncoded(), KEY_ALGORITHM);
    }
    
    /**
     * 设置扰码长度。
     * 
     * @param   scrambleLength  扰码长度
     */
    public void setScrambleLength(int scrambleLength) {
        
        this.scrambleLength = scrambleLength;
    }
    
    /**
     * 加密。
     * 
     * @param   value           要加密的值
     * 
     * @return  值的密文。如果value是null，则返回null。
     */
    public String encrypt(String value) {
        
        if (value == null) return null;
        
        try {
            byte[] scramble1 = this.getRandomString(this.scrambleLength).getBytes(CHARSET);
            byte[] scramble2 = this.getRandomString(this.scrambleLength).getBytes(CHARSET);
            byte[] scramble3 = this.getRandomString(this.scrambleLength).getBytes(CHARSET);
            byte[] scramble4 = this.getRandomString(this.scrambleLength).getBytes(CHARSET);
            byte[] data = value.getBytes(CHARSET);
            byte[] digest = this.md5(data);
            
            // 按照前面定义的扰码、摘要、数据的顺序，将多个字节数组内容拷贝到明文字符数组中。
            byte[] plainText = new byte[this.scrambleLength * 4 + DIGEST_LENGTH + data.length];
            System.arraycopy(scramble1, 0, plainText, 0, this.scrambleLength);
            System.arraycopy(digest, 0, plainText, this.scrambleLength, 5);
            System.arraycopy(scramble2, 0, plainText, this.scrambleLength + 5, this.scrambleLength);
            System.arraycopy(digest, 5, plainText, this.scrambleLength * 2 + 5, 2);
            System.arraycopy(data, 0, plainText, this.scrambleLength * 2 + 7, data.length);
            System.arraycopy(digest, 7, plainText, this.scrambleLength * 2 + 7 + data.length, 6);
            System.arraycopy(scramble3, 0, plainText, this.scrambleLength * 2 + 13 + data.length, this.scrambleLength);
            System.arraycopy(digest, 13, plainText, this.scrambleLength * 3 + 13 + data.length, 3);
            System.arraycopy(scramble4, 0, plainText, this.scrambleLength * 3 + 16 + data.length, this.scrambleLength);
            
            // 对明文加密
            byte[] cipherText = this.encrypt(plainText);
            
//            return this.bytesToHex(cipherText);
            
            return Base64.encodeBase64URLSafeString(cipherText);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(CHARSET + " is not supported!", e);
        } catch (Exception e) {
            throw new RuntimeException("Encryption error!", e);
        }
    }
    
    /**
     * 解密。
     * 
     * @param   value           要解密的密文
     * 
     * @return  值的明文。如果返回null，说明value是null、数据错误或者数据被篡改。
     */
    public String decrypt(String value) {
        
        if ((value == null) || (value.length() == 0)) return value;
        
        try {
            // 解密数据
//            byte[] cipherText = this.hexToBytes(value);     // 获得值的密文字节数组
            byte[] cipherText = Base64.decodeBase64(value); // 获得值的密文字节数组
            byte[] plainText;
            try {
                plainText = this.decrypt(cipherText);       // 解密获得的明文字节数组
            } catch (Exception e) {
                return null;
            }
            
            // 判断明文数据长度，如果小于摘要 + 所有扰码的长度，说明密文不正确。
            if (plainText.length < this.scrambleLength * 4 + DIGEST_LENGTH) return null;
            
            // 截取明文中的数据
            int dataStartPos = this.scrambleLength * 2 + 7; // 数据在明文数据中的起始位置
            int dataEndPos = plainText.length - (this.scrambleLength * 2 + 9);  // 数据在明文数据中的截止位置
            int dataLen = dataEndPos - dataStartPos;        // 数据的字节长度
            byte[] data = new byte[dataLen];
            System.arraycopy(plainText, dataStartPos, data, 0, dataLen);
            
            // 截取明文中的数据摘要
            byte[] digest = new byte[DIGEST_LENGTH];
            System.arraycopy(plainText, this.scrambleLength, digest, 0, 5);
            System.arraycopy(plainText, this.scrambleLength * 2 + 5, digest, 5, 2);
            System.arraycopy(plainText, this.scrambleLength * 2 + 7 + dataLen, digest, 7, 6);
            System.arraycopy(plainText, this.scrambleLength * 3 + 13 + dataLen, digest, 13, 3);
            
            // 校验真实数据是否被篡改
            byte[] dataDigest = this.md5(data);
            if (!Arrays.equals(digest, dataDigest)) return null;
            
            return new String(data, CHARSET);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(CHARSET + " is not supported!", e);
        }
    }
    
    /*
     * 将字节数组转换为十六进制字符串。
     * 
     * @param   b           字节数组
     * 
     * @return  十六进制字符串
     */
    String bytesToHex(byte[] b) {
        
        if (b == null) return null;
        
        int len = b.length;
        StringBuilder result = new StringBuilder(len * 2);
        
        for (int i = 0; i < len; i++) {
            result.append(HEX_CHARS[(b[i] >> 4) & 0x0f]);
            result.append(HEX_CHARS[b[i] & 0x0f]);
        }
        
        return result.toString();
    }
    
    /*
     * 将十六进制字符串转换为字节数组。
     * 
     * @param   s           十六进制字符串
     * 
     * @return  字节数组
     */
    byte[] hexToBytes(String s) {
        
        if ((s == null) || (s.trim().length() == 0)) return null;
        
        int len = s.length() / 2;
        byte[] b = new byte[len];
        
        char[] hexChars = s.toLowerCase().toCharArray();
        for (int i = 0; i < len; i++) {
            int pos = i * 2;
            b[i] = (byte) ((HEX_STRING.indexOf(hexChars[pos]) << 4) | HEX_STRING.indexOf(hexChars[pos + 1]));
        }
        
        return b;
    }
    
    /*
     * 加密数据。
     * 
     * @param   data        待加密数据
     * 
     * @return  密文
     * 
     * @throws  Exception   当加密出问题时抛出
     */
    private byte[] encrypt(byte[] data) throws Exception {
        
        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, this.key);
        
        return cipher.doFinal(data);
    }
    
    /*
     * 解密数据。
     * 
     * @param   data        待解密数据
     * 
     * @return  明文
     * 
     * @throws  Exception   当解密出问题时抛出
     */
    private byte[] decrypt(byte[] data) throws Exception {
        
        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, this.key);
        
        return cipher.doFinal(data);
    }
    
    /*
     * 获得MD5摘要信息。
     * 
     * @param   data        待获取摘要的数据
     * 
     * @return  字节数组的摘要信息。如果bytes是null，则返回null。
     * 
     * @throws  UnsupportedOperationException   如果算法是null、空字符串或者不支持，则抛出。
     */
    private byte[] md5(byte[] data) {
        
        try {
            MessageDigest md = MessageDigest.getInstance(DIGEST_ALGORITHM);
            if (data == null) return null;
            md.update(data);
            return md.digest();
        } catch (NoSuchAlgorithmException e) {
            throw new UnsupportedOperationException("Algorithm[" + DIGEST_ALGORITHM + "] is not supported!", e);
        }
    }
    
    /*
     * 生成指定长度的随机字符串。
     * 
     * @param   length          随机字符串的长度
     * 
     * @return  随机字符串
     */
    public String getRandomString(int length) {
        
        Random random = new Random();
        
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < length; i++) result.append(RANDOM_CHARS.charAt(random.nextInt(RANDOM_CHARS.length())));
        
        return result.toString();
    }
}