package com.kuyun.auditing.rest.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.kuyun.auditing.rest.constants.ErrorCode;
import com.kuyun.auditing.rest.utils.AESCipher;
import com.kuyun.auditing.rest.utils.CookieUtil;
import com.kuyun.auditing.rest.view.ResponseBase;
import com.kuyun.auditing.rest.view.ResponseWrapper;

public class BaseControllor {
	
	protected Logger LOGGER = LoggerFactory.getLogger(BaseControllor.class);
	
	AESCipher aesCipher = new AESCipher("ad_auditing");
	
	@ExceptionHandler(MissingServletRequestParameterException.class)
	@ResponseBody
	ResponseWrapper<Object> handleControllerException(HttpServletRequest request, Exception ex) {
		ResponseBase<Object> response = new ResponseBase<>();
		response.setResultCode(ErrorCode.MISSING_PARAMS);
		ResponseWrapper<Object> res = new ResponseWrapper<>(response);
		return res;
	}
	
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	@ResponseBody
	ResponseWrapper<Object> requestTypeMismatch(MethodArgumentTypeMismatchException ex){
		ResponseBase<Object> response = new ResponseBase<>();
		response.setResultCode(ErrorCode.ILLEGAL_PARAMS);
		ResponseWrapper<Object> res = new ResponseWrapper<>(response);
	    return res;
	}
	
	protected boolean checkLogin(){
		return false;
	}
	
	protected int currentUser(HttpServletRequest request){
		String identity = CookieUtil.getCookie("identity", request);
		String userId = aesCipher.decrypt(identity);
		int userId_ = Integer.parseInt(userId);
		return userId_;
	}
}
