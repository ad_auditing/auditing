package com.kuyun.auditing.rest.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kuyun.auditing.rest.domain.UserInfo;

@Repository
public interface UserRepository extends JpaRepository<UserInfo, Integer> {
	
	public UserInfo findByNameAndPassword(String name,String password);
	
	public UserInfo findByName(String name);
	
}
