package com.kuyun.auditing.rest.controller;

import java.text.Format;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuyun.auditing.rest.constants.AuditingStatus;
import com.kuyun.auditing.rest.constants.Charging;
import com.kuyun.auditing.rest.constants.ErrorCode;
import com.kuyun.auditing.rest.dao.AuditingResultRepository;
import com.kuyun.auditing.rest.dao.MaterialRepository;
import com.kuyun.auditing.rest.dao.UserRepository;
import com.kuyun.auditing.rest.domain.Material;
import com.kuyun.auditing.rest.domain.UserInfo;
import com.kuyun.auditing.rest.domain.AuditingResult;
import com.kuyun.auditing.rest.utils.CookieUtil;
import com.kuyun.auditing.rest.utils.RedisUtil;
import com.kuyun.auditing.rest.utils.SmsUtil;
import com.kuyun.auditing.rest.view.AuditingDetailView;
import com.kuyun.auditing.rest.view.AuditingListView;
import com.kuyun.auditing.rest.view.MaterialView;
import com.kuyun.auditing.rest.view.ProgressInfoView;
import com.kuyun.auditing.rest.view.ResponseBase;
import com.kuyun.auditing.rest.view.ResponseStatus;
import com.kuyun.auditing.rest.view.ResponseWrapper;
import com.kuyun.auditing.rest.view.UserInfoView;

@RestController
public class ApiController extends BaseControllor {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	MaterialRepository materialRepository;
	
	@Autowired
	AuditingResultRepository auditingResultRepository;
	
	@Value("${verification_message_template}")
	String verificationMessageTemplate;
	
	@Value("${default_ad_cpm_price}")
	String adPrice;
	
	String vcachePrefix = "ad_auditing_verification_code_";
	
	String verificationSuccessPrefix = "ad_auditing_verification_success";
	
	@Autowired
	RedisUtil redisUtil;

	@RequestMapping("/auditing/api/user/login")
	public ResponseWrapper<ResponseStatus> login(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse, @RequestParam("user_name") String userName,
			@RequestParam("password") String password, @RequestParam("verification_code") int verificationCode) {
		ResponseBase<ResponseStatus> response = new ResponseBase<>();
		UserInfo user = userRepository.findByNameAndPassword(userName, password);
		if (user != null) {
			response.setData(new ResponseStatus(0, "登录成功"));
			String encryptUser = aesCipher.encrypt(String.valueOf(user.getId()));
			CookieUtil.setCookie("identity", encryptUser, 60*60*2, httpServletRequest, httpServletResponse,null,"/");
		} else {
			response.setData(new ResponseStatus(1, "登录失败"));
		}
		ResponseWrapper<ResponseStatus> res = new ResponseWrapper<>(response);
		return res;
	}

	@RequestMapping("/auditing/api/user/logout")
	public ResponseWrapper<ResponseStatus> logout(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) {
		CookieUtil.cleanCookies(httpServletRequest, httpServletResponse);
		ResponseBase<ResponseStatus> response = new ResponseBase<>();
		response.setData(new ResponseStatus(0, "登出成功"));
		ResponseWrapper<ResponseStatus> res = new ResponseWrapper<>(response);
		return res;
	}
	
	@RequestMapping("/auditing/api/system/get_verification_code")
	public ResponseWrapper<Object> getVerificationCode(HttpServletRequest httpServletRequest) {
		ResponseBase<Object> response = new ResponseBase<>();
		ResponseWrapper<Object> res = new ResponseWrapper<>(response);
		String verificationCode = SmsUtil.genVerificationCode();
		UserInfo user = userRepository.findOne(currentUser(httpServletRequest));
		String redisKey = vcachePrefix + user.getId();
		if (redisUtil.exists(redisKey)) {
			response.setResultCode(ErrorCode.SMS_INTERVAL_SHORT);
			return res;
		}
		String message = MessageFormat.format(verificationMessageTemplate, verificationCode);
		if (!SmsUtil.sms(user.getMobile(), message)) {
			response.setResultCode(ErrorCode.SMS_SEND_ERROR);
			return res;
		}
		long mins5 = 60 * 5;
		if(!redisUtil.set(vcachePrefix + user.getId(), verificationCode, mins5)){
			LOGGER.error("redis write data error , key is " + vcachePrefix + user.getId());
		}
		return res;
	}
	
	@RequestMapping("auditing/api/user/verification")
	public ResponseWrapper<ResponseStatus> verification(@RequestParam("verification_code") String verificationCode,
			HttpServletRequest httpServletRequest) {
		ResponseBase<ResponseStatus> response = new ResponseBase<>();
		ResponseWrapper<ResponseStatus> wrapper = new ResponseWrapper<>(response);
		ResponseStatus status = new ResponseStatus(1);
		int userId = currentUser(httpServletRequest);
		long expireTime = 60 * 5;
		String redisKey = vcachePrefix + userId;
		if(redisUtil.exists(redisKey)){
			String cacheCode = (String)redisUtil.get(redisKey);
			if(verificationCode != null && verificationCode.equals(cacheCode)){
				status.setMessage("验证成功。");
				response.setData(status);
				redisUtil.set(verificationSuccessPrefix + userId, true, expireTime);
			}
		}
		return wrapper;
	}
	
	@RequestMapping("/auditing/api/user/reset_password_result")
	public ResponseWrapper<ResponseStatus> resetPasswordResult(@RequestParam("password") String password,
			HttpServletRequest httpServletRequest){
		ResponseBase<ResponseStatus> response = new ResponseBase<>();
		ResponseWrapper<ResponseStatus> wrapper = new ResponseWrapper<>(response);
		int userId = currentUser(httpServletRequest);
		UserInfo user = userRepository.findOne(currentUser(httpServletRequest));
		ResponseStatus status = new ResponseStatus();
		response.setData(status);
		if(user == null){
			status.setStatus(1);
			status.setMessage("未知用户。");
			return wrapper;
		}
		if(!redisUtil.exists(verificationSuccessPrefix + userId)){
			status.setStatus(1);
			status.setMessage("身份未验证。");
			return wrapper;
		}
		redisUtil.remove(verificationSuccessPrefix + userId);
		user.setPassword(password);
		userRepository.save(user);
		status.setMessage("密码修改成功。");
		return wrapper;
	}
	
	@RequestMapping("/auditing/api/user/get_user_info")
	public ResponseWrapper<UserInfoView> getUserInfo(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse){
		String identity = CookieUtil.getCookie("identity", httpServletRequest);
		String userName = aesCipher.decrypt(identity);
		UserInfo user = userRepository.findByName(userName);
		UserInfoView view = new UserInfoView();
		view.setUserId(user.getId());
		view.setUserName(user.getName());
		view.setMobileNumber(user.getMobile());
		view.setRemarks(user.getRemarks());
		ResponseBase<UserInfoView> response = new ResponseBase<>();
		response.setData(view);
		ResponseWrapper<UserInfoView> res = new ResponseWrapper<>(response);
		return res;
	}
	
	@RequestMapping("/auditing/api/audit/get_audit_list")
	public ResponseWrapper<AuditingListView> getAuditList(@RequestParam("audit_status") Integer status,HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse,Pageable pageable){
		ResponseBase<AuditingListView> response = new ResponseBase<>();
		ResponseWrapper<AuditingListView> wrapper = new ResponseWrapper<>(response);
		Format format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String identity = CookieUtil.getCookie("identity", httpServletRequest);
		String userId = aesCipher.decrypt(identity);
		int userId_ = Integer.parseInt(userId);
		Page<Material> page = null;
		if(status == 0){
			page = materialRepository.findAll(pageable);
		} else if(status == 1){
			page = materialRepository.findByAuditingResultIsNull(pageable);
		} else {
			page = materialRepository.findByResultStatus(status, userId_, pageable);
		}
		List<Material> result = page.getContent();
		AuditingListView viewData = new AuditingListView();
		viewData.setOptions(genOptions());
		viewData.setTotalPages(page.getTotalPages());
		viewData.setPageNumber(page.getNumber());
		viewData.setSize(page.getSize());
		List<MaterialView> list = new ArrayList<>();
		for (Material material : result) {
			MaterialView view = new MaterialView();
			view.setMaterialImg(material.getUrl());
			view.setAuditStatus(AuditingStatus.pending.getCode());
			view.setCharging(Charging.CPM.name());
			view.setMaterialId(material.getId());
			AuditingResult areuslt = containUser(material.getAuditingResult(),userId_);
			if(areuslt != null){
				view.setAuditTime(format.format(areuslt.getUpdateTime()));
				view.setAuditStatus(areuslt.getStatus());
			}
			list.add(view);
		}
		viewData.setList(list);
		response.setData(viewData);
		return wrapper;
	}
	
	@RequestMapping("/auditing/api/audit/get_progress_info")
	public ResponseWrapper<ProgressInfoView> getProgressInfo(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse, Pageable pageable) {
		ResponseBase<ProgressInfoView> response = new ResponseBase<>();
		ResponseWrapper<ProgressInfoView> wrapper = new ResponseWrapper<>(response);
		ProgressInfoView view = new ProgressInfoView();
		int userId = currentUser(httpServletRequest);
		int todayCount = materialRepository.countByAuditingResultAuditingUserAndAuditingResultUpdateTimeAfterAndAuditingResultUpdateTimeBefore(
				userId, getTodayBegin(), getTodayEnd());
		view.setMaterialCount(todayCount);
		int auditedCount = materialRepository.countByAuditingResultAuditingUser(userId);
		view.setAuditedCount(auditedCount);
		view.setWaitAuditCount((int)materialRepository.count() - auditedCount);
		response.setData(view);
		return wrapper;
	}
	
	@RequestMapping("/auditing/api/audit/view")
	public ResponseWrapper<AuditingDetailView> view(@RequestParam("material_id") Integer materialId,HttpServletRequest httpServletRequest){
		Material material = materialRepository.findOne(materialId);
		ResponseBase<AuditingDetailView> response = new ResponseBase<>();
		ResponseWrapper<AuditingDetailView> wrapper = new ResponseWrapper<>(response);
		if(material == null){
			response.setResultCode(ErrorCode.ILLEGAL_PARAMS);
			return wrapper;
		}
		AuditingDetailView data = new AuditingDetailView();
		data.setMaterialId(materialId);
		data.setImage(material.getUrl());
		data.setCharging(Charging.CPM.name());
		data.setPrice(adPrice);
		data.setDiagrams(genDiagrams(material.getGenPicUrl()));
		List<AuditingResult> auditingSet = material.getAuditingResult();
		if(auditingSet.size() > 1){
			LOGGER.error("Auditing result data error.");
		}
		if(auditingSet.size() > 0){
			AuditingResult auditingResult = auditingSet.get(0);
			data.setStatus(auditingResult.getStatus());
			data.setReason(auditingResult.getRefuse());
			data.setExculdeTime(auditingResult.getExculdeTime());
			data.setMem(auditingResult.getFailReason());
		}
		response.setData(data);
		return wrapper;
	}
	
	private List<String> genDiagrams(String genPicUrl) {
		if(genPicUrl == null || genPicUrl.equals("")){
			return null;
		}
		String[] array = genPicUrl.split(";");
		List<String> urls = new ArrayList<>();
		String baseUrl = null;
		for (int i = 0; i < array.length; i++) {
			String ele = array[i];
			if(i == 0){
				baseUrl = ele;
			} else {
				urls.add(baseUrl + "/" + ele);
			}
			
		}
		return urls;
	}

	@RequestMapping("/auditing/api/list/refuse")
	public ResponseWrapper<ResponseStatus> refuse(@RequestParam("material_id") Integer materialId,HttpServletRequest httpServletRequest,
			Integer reason,String mem,
			HttpServletResponse httpServletResponse) {
		int userId = currentUser(httpServletRequest);
		ResponseBase<ResponseStatus> response = new ResponseBase<>();
		ResponseWrapper<ResponseStatus> wrapper = new ResponseWrapper<>(response);
		Material material = materialRepository.findOne(materialId);
		ResponseStatus status = new ResponseStatus();
		response.setData(status);
		if (material == null) {
			status.setStatus(1);
			return wrapper;
		}
		AuditingResult  result = auditingResultRepository.findByMaterialIdAndAuditingUser(materialId, userId);
		if(result == null){
			result = new AuditingResult();
		}
		if(result.getStatus() == AuditingStatus.approval.getCode()){
			status.setStatus(1);
			return wrapper;
		}
		result.setAuditingUser(userId);
		result.setMaterial(material);
		result.setStatus(AuditingStatus.refuse.getCode());
		if(reason != null){
			result.setRefuse(reason.intValue());
		}
		result.setFailReason(mem);
		result.setUpdateTime(new Date());
		auditingResultRepository.save(result);
		return wrapper;
	}
	
	@RequestMapping("auditing/api/list/pass")
	public ResponseWrapper<ResponseStatus> pass(@RequestParam("material_id") Integer materialId,HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) {
		int userId = currentUser(httpServletRequest);
		ResponseBase<ResponseStatus> response = new ResponseBase<>();
		ResponseWrapper<ResponseStatus> wrapper = new ResponseWrapper<>(response);
		Material material = materialRepository.findOne(materialId);
		ResponseStatus status = new ResponseStatus();
		response.setData(status);
		if(material == null){
			status.setStatus(1);
			return wrapper;
		}
		AuditingResult  result = auditingResultRepository.findByMaterialIdAndAuditingUser(materialId, userId);
		if(result == null){
			result = new AuditingResult();
		}
		result.setAuditingUser(userId);
		result.setMaterial(material);
		result.setStatus(AuditingStatus.approval.getCode());
		result.setUpdateTime(new Date());
		auditingResultRepository.save(result);
		return wrapper;
	}
	
	private Date getTodayBegin(){
		Calendar todayStart = Calendar.getInstance();  
        todayStart.set(Calendar.HOUR, 0);  
        todayStart.set(Calendar.MINUTE, 0);  
        todayStart.set(Calendar.SECOND, 0);  
        todayStart.set(Calendar.MILLISECOND, 0);  
        return todayStart.getTime();
	}
	
	private Date getTodayEnd(){
        Calendar todayEnd = Calendar.getInstance();  
        todayEnd.set(Calendar.HOUR, 23);  
        todayEnd.set(Calendar.MINUTE, 59);  
        todayEnd.set(Calendar.SECOND, 59);  
        todayEnd.set(Calendar.MILLISECOND, 999);  
        return todayEnd.getTime();
	}
	
	private Map<String, Map<Integer, String>> genOptions(){
		Map<String, Map<Integer, String>> options = new HashMap<>();
		Map<Integer, String> status = new TreeMap<>();
		options.put("audit_status", status);
		status.put(AuditingStatus.pending.getCode(), AuditingStatus.pending.getValue());
		status.put(AuditingStatus.approval.getCode(), AuditingStatus.approval.getValue());
		status.put(AuditingStatus.refuse.getCode(), AuditingStatus.refuse.getValue());
		return options;
	}
	
	private AuditingResult containUser(List<AuditingResult> result ,int userId){
		for (AuditingResult auditingResult : result) {
			if(auditingResult.getAuditingUser() == userId){
				return auditingResult;
			}
		}
		return null;
	}

}
