package com.kuyun.auditing.rest.view;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseWrapper <T> {
	
	@JsonProperty("Response")
	ResponseBase<T> response;
	
	public ResponseWrapper(ResponseBase<T> content) {
		this.response = content;
	}

	public ResponseBase<T> getResponse() {
		return response;
	}

	public void setResponse(ResponseBase<T> response) {
		this.response = response;
	}
}
