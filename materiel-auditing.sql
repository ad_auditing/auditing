SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS approval_rules;
DROP TABLE IF EXISTS auditing_result;
DROP TABLE IF EXISTS code_position;
DROP TABLE IF EXISTS user_info;




/* Create Tables */

CREATE TABLE approval_rules
(
	id int NOT NULL AUTO_INCREMENT,
	auditing_id int unsigned NOT NULL,
	exclude varchar(1000) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (auditing_id)
);


CREATE TABLE auditing_result
(
	id int NOT NULL AUTO_INCREMENT,
	resource_id int unsigned NOT NULL,
	-- 审核结果
	-- 
	-- 待审核、审核通过、审核拒绝
	status tinyint DEFAULT 0 NOT NULL COMMENT '审核结果

待审核、审核通过、审核拒绝',
	auditing_user int unsigned NOT NULL,
	fail_reason varchar(500),
	-- 审核时间
	update_time datetime NOT NULL COMMENT '审核时间',
	PRIMARY KEY (id)
) ENGINE = InnoDB;


CREATE TABLE code_position
(
	id int NOT NULL AUTO_INCREMENT,
	role tinyint NOT NULL,
	role_id varchar(200),
	code_postion int,
	PRIMARY KEY (id)
);


CREATE TABLE user_info
(
	id int unsigned NOT NULL AUTO_INCREMENT,
	name varchar(20) DEFAULT '' NOT NULL,
	-- md5
	password varchar(50) NOT NULL COMMENT 'md5',
	-- 角色区分
	-- 1终端厂商
	-- 2电视台
	role tinyint unsigned DEFAULT 0 NOT NULL COMMENT '角色区分
1终端厂商
2电视台',
	-- 是否可用
	status tinyint DEFAULT 0 NOT NULL COMMENT '是否可用',
	-- 用户创建时间
	create_time datetime DEFAULT NOW(), SYSDATE() NOT NULL COMMENT '用户创建时间',
	-- 最后登陆时间
	last_login_time datetime COMMENT '最后登陆时间',
	-- 厂商分类id
	-- 电视台分类id
	-- 对于统一厂商/电视台用户使用0表示
	role_category int COMMENT '厂商分类id
电视台分类id
对于统一厂商/电视台用户使用0表示',
	mobile varchar(11),
	mail varchar(100),
	remarks varchar(200),
	PRIMARY KEY (id),
	UNIQUE (name)
);



/* Create Foreign Keys */

ALTER TABLE approval_rules
	ADD FOREIGN KEY (auditing_id)
	REFERENCES auditing_result (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE auditing_result
	ADD FOREIGN KEY (auditing_user)
	REFERENCES user_info (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



